package day03;

class Parent1 {
		public Parent1() {
			System.out.println("Parent Class Default Constructor Invoked");
		}
		
		public Parent1(int x) {
			System.out.println("Parent Class Parameterised Constructor Invoked");
		}
	}

	public class ChildDemo1 extends Parent1 {
		
		public ChildDemo1() {
			System.out.println("Child  Class Default Constructor Invoked");
		}
		
		public ChildDemo1(int x) {
			super(x);
			System.out.println("Child  Class Parameterised Constructor Invoked");
		}
		
		public static void main(String[] args) {
			ChildDemo1 obj1 = new ChildDemo1();
			ChildDemo1 obj2 = new ChildDemo1(25);
			
		}
	}