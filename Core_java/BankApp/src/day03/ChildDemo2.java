package day03;

class Parent {	
	int x;
	int y;
	
	public Parent(int x, int y) {
		System.out.println("Default Constructor from Parent Class");
		this.x = x;
		this.y = y;
	}
	
	public void showParent() {
		System.out.println("showParent() from Parent Class");
		System.out.println("x = " + x + "\ny = " + y + "\n");
	}
}

class Child extends Parent {
	
	int z;
	
	public Child(int x, int y, int z) {
		super(x, y);
		System.out.println("Default Constructor from Child Class");
		this.z = z;
	}
	
	public void showChild() {
		System.out.println("showChild() from Child Class");
		
		this.z = this.x + this.y;
		System.out.println("z = " + z);
	}
}

public class ChildDemo2 {

	public static void main(String[] args) {
		Child obj = new Child(10, 20, 11);
		obj.showParent();
		obj.showChild();

	}

}
