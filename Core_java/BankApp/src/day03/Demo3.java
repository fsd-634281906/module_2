package day03;

import java.util.Scanner;

//custom /user defined exception

class OddNumberException extends Exception{
	
	public OddNumberException(String msg){
		super(msg);
	}
	
}

public class Demo3 {

	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		
		System.out.println("Enter a number: ");
		int num = scan.nextInt();
		System.out.println();
		
		try{
			if (num % 2 != 0){
				OddNumberException obj = new OddNumberException("Cannot Enter Odd Number");
				throw obj;
			} else {
				System.out.println("Entered Number is an Even Number");
			}
		} catch (OddNumberException ex) {
			System.out.println(ex);
			System.out.println();
		}
		
		
		System.out.println("\nHi All! \n");
	}
}
