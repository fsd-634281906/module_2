package day03;

import java.util.Scanner;

public class ExceptionDemo {

	public static void main(String[] args) {
		System.out.println("Program Started");
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the inputs");
		int num1 = sc.nextInt();
		int num2 = sc.nextInt();
		int[] arr = new int[4];
		System.out.println("Sum : " + (num1 + num2));
		try{
			System.out.println("Diff : " + (num1 - num2));
			System.out.println("Multiplication : " + (num1 * num2));
			System.out.println("Division : " + (num1 / num2));
		} catch (ArithmeticException e){
			System.out.println(e.getMessage());
		} catch (ArrayIndexOutOfBoundsException ae) {
			System.out.println(ae.getMessage());
		} finally {
			System.out.println("Finally Block");
		}
		System.out.println("Program Ended");
		
		
		

	}

}
