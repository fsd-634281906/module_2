package day04;

public class PersonalCustomer extends Customer{
	
	private long homePhone;
	private long officePhone;
	public PersonalCustomer() {
		super();
		
		
	}
	public PersonalCustomer(String firstName, String lastName, String address, double balance, long homePhone, long officePhone) {
		super(firstName, lastName, address, balance);
		
		this.homePhone = homePhone;
		this.officePhone = officePhone;
	}
	@Override
	public String toString() {
		return "PersonalCustomer [customerId=" + customerId + 
				", firstName=" + firstName + ", lastName=" + lastName + 
				", address=" + address + ", balance=" + balance + 
				", officePhone=" + officePhone + 
				", homePhone=" + homePhone + "]";
	}
	
	
	
	
	
	

}
