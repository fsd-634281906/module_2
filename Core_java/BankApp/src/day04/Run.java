package day04;

import java.util.Scanner;

public class Run {

	public static void main(String[] args) {
		int customerId = 0;
		Bank bank = new Bank();
		Scanner scan = new Scanner(System.in);
				
		while (true) {
			System.out.println("\n");
			System.out.println("Select Your Choice");
			System.out.println("******************");
			System.out.println("1. Create Commercial Customer");
			System.out.println("2. Create Commercial Customer");
			System.out.println("3. Show Customers");
			System.out.println("4. Show Customer By Id");
			System.out.println("5. Deposit");
			System.out.println("6. WithDraw");
			System.out.println("7. Check Balance");
			System.out.println("8. Fund Transfer");
			System.out.println("9. Exit \n");
			
			System.out.print("Enter Your Choice: ");
			int choice = scan.nextInt();
			System.out.println();
			
			switch (choice) {
			case 1:
				System.out.print("Enter First Name: ");
				String firstName = scan.next();
				
				System.out.print("Enter Last  Name: ");
				String lastName = scan.next();
				
				System.out.print("Enter Address   : ");
				String address = scan.next();
				
				System.out.print("Enter Balance   : ");
				double balance = scan.nextDouble();
				
				System.out.print("Enter ContactPerson Name  : ");
				String contactPersonName = scan.next();
				
				System.out.print("Enter ContactPerson Number  : ");
				Long contactPersonNumber = scan.nextLong();
				
				CommercialCustomer commercialCustomer = new CommercialCustomer(firstName, lastName, address, balance, contactPersonName, contactPersonNumber);
				String msg = bank.addCommercialCustomer(commercialCustomer);
				System.out.println(msg);
			break;
			
			case 2:
				
			break;
			
			case 3:
				bank.showCustomers();
			break;
			
			case 4:
				System.out.print("Enter Customer ID: ");
				customerId = scan.nextInt();
				bank.showCustomerById(customerId);
			break;
			
			case 5:
				System.out.println("Enter Customer ID: ");
				customerId = scan.nextInt();
				System.out.println("Enter the amount you want to Deposit: ");
				int deposit = scan.nextInt();
				bank.depositMoney(customerId,deposit);
			break;
			
			case 6:
				System.out.println("Enter Customer ID: ");
				customerId = scan.nextInt();
				System.out.println("Enter the amount you want to withdraw: ");
				int withdraw = scan.nextInt();
				bank.withdrawMoney(customerId,withdraw);
			break;
						
			case 7:
				System.out.print("Enter Customer ID: ");
				customerId = scan.nextInt();
				bank.showBalance(customerId);
			break;
			case 8:
				System.out.println("Transfer From ID:");
				int fromId = scan.nextInt();
				System.out.println("Transfer To ID:");
				int toId = scan.nextInt();
				System.out.println("Enter the amount to be Transferred: ");
				int amount = scan.nextInt();
				bank.fundTransfer(fromId, toId, amount);
			break;
			
			
			case 9: System.out.println("Thank You.");
					System.out.println("Application Terminated...\n");
					System.exit(0);
			break;
			
			default: System.out.println("Invalid Chioce \n");
			break;
			}
		}
		
		
	}
}

