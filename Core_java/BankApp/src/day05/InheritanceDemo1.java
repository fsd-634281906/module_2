//package day05;
//
////1.Inheritance 
//
//class Car {
//	public void Steering() {
//		System.out.println("Steering: Manual");
//	}
//	public void Brakes() {
//		System.out.println("Brakes  : Drum Brakes");
//	}
//	public void Color() {
//		System.out.println("Color   : White");
//	}
//}
//
//class Swift extends Car {
//}
//
//class Creta extends Car {
//}
//
//public class InheritanceDemo1 {
//	public static void main(String[] args) {
//		
//		System.out.println("Swift Car Specifications");
//		System.out.println("------------------------");
//		Swift swift = new Swift();
//		swift.Color();
//		swift.Brakes();
//		swift.Steering();
//		System.out.println();
//		
//		
//		System.out.println("Creta Car Specifications");
//		System.out.println("------------------------");
//		Creta creta = new Creta();
//		creta.Color();
//		creta.Brakes();
//		creta.Steering();
//		System.out.println();
//	}
//}
//package day05;
//
////1.Inheritance 
//
//class Car {
//	public void Steering() {
//		System.out.println("Steering: Manual");
//	}
//	public void Brakes() {
//		System.out.println("Brakes  : Drum Brakes");
//	}
//	public void Color() {
//		System.out.println("Color   : White");
//	}
//}
//
//class Swift extends Car {
//	public void MusicSystem() {
//		System.out.println("Music   : Sony Music System");
//	}
//}
//
//class Creta extends Car {
//	public void MusicSystem() {
//		System.out.println("Music   : Bose Music System");
//	}
//}
//
//public class InheritanceDemo1 {
//	public static void main(String[] args) {
//		
//		System.out.println("Swift Car Specifications");
//		System.out.println("------------------------");
//		Swift swift = new Swift();
//		swift.Color();
//		swift.Brakes();
//		swift.Steering();
//		swift.MusicSystem();
//		System.out.println();
//		
//		
//		System.out.println("Creta Car Specifications");
//		System.out.println("------------------------");
//		Creta creta = new Creta();
//		creta.Color();
//		creta.Brakes();
//		creta.Steering();
//		creta.MusicSystem();
//		System.out.println();
//	}
//}
//
//package day05;
//
////2.Overriding
//
//class Car {
//	public void steering() {
//		System.out.println("Steering: Manual");
//	}
//	public void brakes() {
//		System.out.println("Brakes  : Drum Brakes");
//	}
//	public void color() {
//		System.out.println("Color   : White");
//	}
//}
//
//class Swift extends Car {
//	
//	@Override
//	public void color() {
//		System.out.println("Color   : Red");
//	}
//	
//	public void musicSystem() {
//		System.out.println("Music   : Sony Music System");
//	}
//}
//
//class Creta extends Car {
//	
//	@Override
//	public void brakes() {
//		System.out.println("Brakes  : Disk Brakes");
//	}
//	
//	public void musicSystem() {
//		System.out.println("Music   : Bose Music System");
//	}
//}
//
//public class InheritanceDemo1 {
//	public static void main(String[] args) {
//		
//		System.out.println("Swift Car Specifications");
//		System.out.println("------------------------");
//		Swift swift = new Swift();
//		swift.color();
//		swift.brakes();
//		swift.steering();
//		swift.musicSystem();
//		System.out.println();
//		
//		
//		System.out.println("Creta Car Specifications");
//		System.out.println("------------------------");
//		Creta creta = new Creta();
//		creta.color();
//		creta.brakes();
//		creta.steering();
//		creta.musicSystem();
//		System.out.println();
//	}
//}
//
package day05;

//3.Abstract Class

abstract class Car {
	
	//Non-Abstract Method or Concrete Method
	public void steering() {
		System.out.println("Steering: Manual");
	}
	
	//Non-Abstract Method or Concrete Method
	public void brakes() {
		System.out.println("Brakes  : Drum Brakes");
	}
	
	//Abstract Method
	public abstract void color();
}

class Swift extends Car {

	//Parent's Class Abstract Method Defined in Child Class
	@Override
	public void color() {
		System.out.println("Color   : Red Color");
	}
	
	public void musicSystem() {
		System.out.println("Music   : Sony Music System");
	}
}

class Creta extends Car {
		
	@Override
	public void brakes() {
		System.out.println("Brakes  : Disk Brakes");
	}
	
	public void musicSystem() {
		System.out.println("Music   : Bose Music System");
	}

	//Parent's Class Abstract Method Defined in Child Class
	@Override
	public void color() {
		System.out.println("Color   : Maroon Color");
	}
}

public class InheritanceDemo1 {
	public static void main(String[] args) {
		
		System.out.println("Swift Car Specifications");
		System.out.println("------------------------");
		Swift swift = new Swift();
		swift.color();
		swift.brakes();
		swift.steering();
		swift.musicSystem();
		System.out.println();
		
		
		System.out.println("Creta Car Specifications");
		System.out.println("------------------------");
		Creta creta = new Creta();
		creta.color();
		creta.brakes();
		creta.steering();
		creta.musicSystem();
		System.out.println();
	}
}
//
//
