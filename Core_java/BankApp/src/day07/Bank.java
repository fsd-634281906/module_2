package day07;

import java.util.ArrayList;
import java.util.List;

public class Bank {

	int index = 0;
	List<Customer> customerList = new ArrayList<Customer>();


	public String addCommercialCustomer(CommercialCustomer commercialCustomer) {

		Customer customer = commercialCustomer;
		customerList.add(customer);
		
		return "Commercial Customer Addedd Successfully\nCustomerId: " + customer.getCustomerId();
	}

	public String addPersonalCustomer(PersonalCustomer personalCustomer) {
		Customer customer = personalCustomer;
		customerList.add(customer);
		
		return "Personal Customer Addedd Successfully\nCustomerId: " + customer.getCustomerId();
	}


	public List<Customer> showCustomers() {
		return customerList;
	}

	public Customer showCustomerById(int customerId) {

		for (Customer customer : customerList) {
			if (customer.getCustomerId() == customerId) {
				return customer;
			}
		}
		
		return null;
	}

	public boolean isCustomerExists(int customerId) {		
		for (Customer customer : customerList) {
			if (customer.getCustomerId() == customerId) {
				return true;
			}
		}
		
		return false;
	}

	public Customer getCustomer(int customerId) {
		for (Customer customer : customerList) {
			if (customer.getCustomerId() == customerId) {
				return customer;
			}
		}
		
		return null;
	}

	public String deposite(int customerId, double amount) {
		Customer customer = getCustomer(customerId);
		String msg = "";

		if (customer != null) {
			customer.setBalance(customer.getBalance() + amount);
			msg += "Amount Deposited: " + amount + "\n";
			msg += "New Balance     : " + customer.getBalance() + "\n";
			return msg;
		} 

		return "Please Provide the Valid CustomerId";
	}

	public String withdraw(int customerId, double amount) {
		Customer customer = getCustomer(customerId);
		String msg = "";

		if (customer != null) {
			if (customer.getBalance() > amount) {
				customer.setBalance(customer.getBalance() - amount);
				msg += "Amount Withdraw : " + amount + "\n";
				msg += "New Balance     : " + customer.getBalance() + "\n";
				return msg;
			}
				
			return "Insufficient Balance!!!";			
		} 

		return "Please Provide the Valid CustomerId";
	}

	public String getBalance(int customerId) {
		Customer customer = getCustomer(customerId);

		if (customer != null) {
			return "Balance: " + customer.getBalance();
		} 

		return "Please Provide the valid CustomerId";
	}

	public String fundTransfer(int customerId, int beneficiaryID, double amount) {

		Customer customer = getCustomer(customerId);		
		if (customer == null)
			return "Please Provide the valid CustomerId";

		Customer beneficiary = getCustomer(beneficiaryID);		
		if (beneficiary == null)
			return "Please Provide the valid BeneficiaryID";

		if (customer.getBalance() < amount)
			return "Insufficient Funds!!!";

		customer.setBalance(customer.getBalance() - amount);
		beneficiary.setBalance(beneficiary.getBalance() + amount);

		return "Amount Transfered Successfully!!!!\n" + 
		"Your New Balance: " + customer.getBalance() + "\n";
	}



}
