package day07;

import java.util.HashSet;
import java.util.Set;

public class HashSetDemo1 {

	public static void main(String[] args) {
		Set set = new HashSet();
		
		set.add(10);
		set.add("hello");
		set.add(true);
		set.add(30);
		set.add(false);
		System.out.println(set);
		
		for(Object obj: set){
			System.out.println(obj);
		}
		
	}

}
