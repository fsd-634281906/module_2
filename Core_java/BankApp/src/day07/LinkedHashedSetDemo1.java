package day07;

import java.util.LinkedHashSet;
import java.util.Set;

public class LinkedHashedSetDemo1 {

	public static void main(String[] args) {
		
		Set set = new LinkedHashSet();
		
		set.add(10);
		set.add(30);
		set.add(20);
		set.add(30);
		set.add(20);
		System.out.println(set);
		
		for(Object obj: set){
			System.out.println(obj);
		}

	}

}
