package day07;

import java.util.List;
import java.util.Scanner;

public class Run {
	
	public static void main(String[] args) {
		int customerId = 0;
		double amount = 0.0;
		Bank bank = new Bank();
		Scanner scan = new Scanner(System.in);
				
		while (true) {
			System.out.println("\n");
			System.out.println("Select Your Choice");
			System.out.println("******************");
			System.out.println("1. Create Commercial Customer");
			System.out.println("2. Create Personal Customer");
			System.out.println("3. Show Customers");
			System.out.println("4. Show Customer By Id");
			System.out.println("5. Deposit");
			System.out.println("6. WithDraw");
			System.out.println("7. Check Balance");
			System.out.println("8. Fund Transfer");
			System.out.println("9. Exit \n");
			
			System.out.print("Enter Your Choice: ");
			int choice = scan.nextInt();
			System.out.println();
			
			switch (choice) {
			
			case 1:	//Add Commercial Customer
				
				System.out.print("Enter First Name    : ");
				String firstName = scan.next();
				
				System.out.print("Enter Last  Name    : ");
				String lastName = scan.next();
				
				System.out.print("Enter Address       : ");
				String address = scan.next();
				
				System.out.print("Enter Balance       : ");
				double balance = scan.nextDouble();
				
				System.out.print("ContactPerson Name  :  ");
				String contactPersonName = scan.next();
				
				System.out.print("ContactPerson Number:  ");
				long contactPersonNumber = scan.nextLong();
				
				CommercialCustomer commercialCustomer = new CommercialCustomer
				(firstName, lastName, address, balance, contactPersonName, contactPersonNumber);
				
				String msg = bank.addCommercialCustomer(commercialCustomer);
				System.out.println(msg);
				
			break;
			
			case 2:	//Add Personal Customer
				System.out.println("Enter FirstName, LastName, Address, Balance, OfficePhone, HomePhone");
				PersonalCustomer personalCustomer = new PersonalCustomer(
						scan.next(), scan.next(), scan.next(), 
						scan.nextDouble(), scan.nextLong(), scan.nextLong());
				
				msg = bank.addPersonalCustomer(personalCustomer);
				System.out.println(msg);
				
			break;
			
			case 3: //Show Customers
				List<Customer> customerList = bank.showCustomers();
				if (customerList.size() > 0) {
					for (Customer customer : customerList) {
						System.out.println(customer);
					}
				} else {
					System.out.println("No Customers Found!!!");
				}
			break;
			
			case 4: //Show Customer By Id
				System.out.print("Enter Customer ID: ");
				customerId = scan.nextInt();
				
				Customer customer = bank.showCustomerById(customerId);
				
				if (customer != null) {
					System.out.println(customer);
				} else {
					System.out.println("Customer Not Found!!");
				}
			break;
			
			case 5://Deposite
				System.out.print("Enter Customer ID: ");
				customerId = scan.nextInt();
				System.out.print("Enter Amount     : ");
				amount = scan.nextDouble();
				msg = bank.deposite(customerId, amount);
				System.out.println(msg);
			break;
			
			case 6://Withdraw
				System.out.print("Enter Customer ID: ");
				customerId = scan.nextInt();
				System.out.print("Enter Amount     : ");
				amount = scan.nextDouble();
				msg = bank.withdraw(customerId, amount);
				System.out.println(msg);
			break;
			
			case 7: //Check Balance
				System.out.print("Enter Customer ID: ");
				customerId = scan.nextInt();
				msg = bank.getBalance(customerId);
				System.out.println(msg);
			break;
			
			case 8: //Fund Transfer
				
				System.out.print("Enter Customer   ID: ");
				customerId = scan.nextInt();				
				
				System.out.print("Enter BeneficiaryID: ");
				int beneficiaryID = scan.nextInt();
				
				System.out.print("Enter the Amount to be Transfered: ");
				double transferAmount = scan.nextDouble();
				
				msg = bank.fundTransfer(customerId, beneficiaryID, transferAmount);
				System.out.println(msg);				
			break;
			
			case 9: System.out.println("Thank You.");
					System.out.println("Application Terminated...\n");
					System.exit(0);
			break;
			
			default: System.out.println("Invalid Chioce \n");
			break;
			}
		}
		
		
	}
}

