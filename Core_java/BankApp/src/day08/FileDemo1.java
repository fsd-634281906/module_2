package day08;

import java.io.File;
import java.util.Date;

public class FileDemo1 {
	public static void main(String[] args) {
		
		File file = new File("myFile1.txt");
		//File file = new File("test");

		
		if (file.isDirectory()) {
			System.out.println("It is a Directory");
			System.out.println("Directory   : " + file.isDirectory());
			
			File files[] = file.listFiles();
			
			for (File f : files) {
				System.out.println(f.getName());
			}
			
		} else {
			System.out.println("It is a File");
			System.out.println("Exists   : " + file.exists());
			System.out.println("getName  : " + file.getName());
			System.out.println("Path     : " + file.getPath());
			System.out.println("CanRead  : " + file.canRead());
			System.out.println("canWrite : " + file.canWrite());
			System.out.println("isFile   : " + file.isFile());
			System.out.println("isHidden : " + file.isHidden());
			System.out.println("lastModified : " + new Date(file.lastModified()));
			System.out.println("Length   : " + file.length());
			System.out.println("Size     : " + file.length() + " bytes");
			System.out.println();
			
			file.setReadOnly();
			System.out.println("canWrite : " + file.canWrite());
			
			file.setWritable(true);
			System.out.println("canWrite : " + file.canWrite());
			

			System.out.println("Exists   : " + file.exists());
			file.delete();
			System.out.println("Exists   : " + file.exists());
		}
		
	}
}
