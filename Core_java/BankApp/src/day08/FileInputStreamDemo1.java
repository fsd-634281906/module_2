package day08;

import java.io.FileInputStream;
import java.io.IOException;

public class FileInputStreamDemo1 {
	public static void main(String[] args) throws IOException {

		FileInputStream fis = new FileInputStream("File1.txt");
		int data;
		
		while ((data = fis.read()) != -1) {
			System.out.print((char) data);
		}
		
		fis.close();
	}
}