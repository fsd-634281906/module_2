package day08;

import java.io.FileOutputStream;
import java.io.IOException;

public class FileOutputStreamDemo1 {
	public static void main(String[] args) throws IOException {
		
		String msg = "Line number 4\n";
		byte info[] = msg.getBytes();
		
		FileOutputStream fos = new FileOutputStream("File1.txt", true);
		fos.write(info);
		
		System.out.println("Done with writing the data to the file");
		fos.close();
	}
}