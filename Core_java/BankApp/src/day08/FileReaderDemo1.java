
package day08;

import java.io.FileReader;
import java.io.IOException;

//public class FileReaderDemo1 {
//	public static void main(String[] args) throws IOException {
//		
//		FileReader fileReader = new FileReader("myFile1.txt");
//		
//		int data = fileReader.read();
//		System.out.println(data);
//		System.out.println( (char) data);
//	}
//}

//public class FileReaderDemo1 {
//	public static void main(String[] args) throws IOException {
//		
//		FileReader fileReader = new FileReader("myFile1.txt");
//		
//		int data = fileReader.read();
//		System.out.print((char)data);
//		
//		data = fileReader.read();
//		System.out.print((char)data);
//		
//		data = fileReader.read();
//		System.out.print((char)data);
//	}
//}

public class FileReaderDemo1 {
	public static void main(String[] args) throws IOException {
		
		FileReader fileReader = new FileReader("myFile1.txt");
		
		int data;
		
		while ((data = fileReader.read()) != -1) {
			System.out.print((char)data);
		}
	}
}