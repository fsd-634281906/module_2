package day08;

import java.io.FileWriter;
import java.io.IOException;

public class FileWriterDemo1 {

	public static void main(String[] args) throws IOException {
		
		FileWriter fileWriter = new FileWriter("myFile1.txt");
		fileWriter.write("Welcome to Files");
		fileWriter.close();
		
		System.out.println("File Created Succesfully");
	}

}