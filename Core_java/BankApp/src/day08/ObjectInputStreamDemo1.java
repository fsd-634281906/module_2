package day08;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;

public class ObjectInputStreamDemo1 {
	public static void main(String[] args) throws IOException, ClassNotFoundException {

		FileInputStream fis = new FileInputStream("ObjectData.txt");
		ObjectInputStream ois = new ObjectInputStream(fis);
		
		Student student1 = (Student) ois.readObject();
		System.out.println(student1);
		
		ois.close();
		fis.close();
	}
}