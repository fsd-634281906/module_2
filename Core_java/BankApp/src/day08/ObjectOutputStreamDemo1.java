
package day08;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;

public class ObjectOutputStreamDemo1 {
	public static void main(String[] args) throws IOException {

		Student student1 = new Student(101, "Harsha", "Java");
		
		FileOutputStream fos = new FileOutputStream("ObjectData.txt");
		ObjectOutputStream oos = new ObjectOutputStream(fos);
		
		oos.writeObject(student1);
		System.out.println("Student Object Written to the File");
		
		oos.close();
		fos.close();
	}
}