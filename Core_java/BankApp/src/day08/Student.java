package day08;

import java.io.Serializable;

public class Student implements Serializable {
	
	private int studId;
	private String sname;
	private String course;
	
	public Student() {
	}

	public Student(int studId, String sname, String course) {
		this.studId = studId;
		this.sname = sname;
		this.course = course;
	}

	public int getStudId() {
		return studId;
	}
	public void setStudId(int studId) {
		this.studId = studId;
	}

	public String getSname() {
		return sname;
	}
	public void setSname(String sname) {
		this.sname = sname;
	}

	public String getCourse() {
		return course;
	}
	public void setCourse(String course) {
		this.course = course;
	}

	@Override
	public String toString() {
		return "Student [studId=" + studId + ", sname=" + sname + ", course=" + course + "]";
	}
}
