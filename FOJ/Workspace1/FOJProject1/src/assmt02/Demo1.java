package assmt02;

public class Demo1 {
	
	public static String fizzBuzz(int num){
		
		String result = "";
		
		if (num % 3 ==0)
			result += "Fizz";
		
		if (num % 5 ==0)
			result += "Bizz";
		
		return result;
		
//		if(num % 3 == 0 && num % 5 == 0){
//			return "fizz-buzz";
//		}
//		else if(num % 5 ==0){
//			return "buzz";
//		}
//		else if(num % 3 == 0){
//			return "fizz";
//		}else{
//			return "Not fizz or buzz";
//		}
	}
	
	public static int sumOfThreeDigits(int num){
		return num/100 + (num / 10) % 10 + num % 10;
		
	}
	
	public static int round(double num){
		int intpart = (int) num;
		double decpart = num - intpart;
		
		if(decpart < 0.5){
			return intpart;
		}else{
			return intpart + 1;
		}
		
	}
	
	public static boolean palindromeTwoDigit(int num){
		return num % 11 == 0;
		
//		int ld = num % 10;
//		int fd = num / 10;
//		
//		if(num < 10 || num > 99){
//			return "Not a two digit number";
//		}
//		
//		if(ld == fd){
//			return "Palindrome";
//		}else{
//			return "Not a palindrome";
//		}
		
	}
	
	public static boolean palindromeThreeDigit(int num){
		return (num / 100) == (num % 10);
//		int fd = num / 100;
//		int ld = num % 10;
//		
//		if(num < 99 || num > 999){
//			return "Not a three digit number";
//		}
//		
//		if(ld == fd){
//			return "Palindrome";
//		}else{
//			return "Not a palindrome";
//		}
		
	}
	

	public static void main(String[] args) {
		System.out.println(fizzBuzz(15));
		System.out.println("The sum of digits of the three digit number is : " + sumOfThreeDigits(111));
		System.out.println("Round of the number is : " + round(1.5));
		System.out.println(palindromeTwoDigit(22));
		System.out.println(palindromeThreeDigit(121));
		
		

	}

}
