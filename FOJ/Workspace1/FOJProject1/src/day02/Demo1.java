package day02;

public class Demo1 {

	public static void main(String[] args) {
		int num1 = 10;
		int num2 = 3;
		
		System.out.println("Sum = " + (num1 + num2));
		System.out.println("Subtraction = " + (num1 - num2));
		System.out.println("Multiplication = " + (num1 * num2));
		System.out.println("Quotient = " + (num1 / num2));
		System.out.println("Reminder = " + (num1 % num2));

	}

}
