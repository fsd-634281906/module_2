package day02;

public class Demo6 {

	public static void main(String[] args) {
		
		String stringValue = "25";
		byte byteValue = Byte.parseByte(stringValue);
		short shortValue = Short.parseShort(stringValue);
		int intValue = Integer.parseInt(stringValue);
		long longValue= Long.parseLong(stringValue);
		
		//Lower to Higher
		System.out.println("stringValue s : " + stringValue);
		System.out.println("byteValue b : " + byteValue );
		System.out.println("shortValue sh : " + shortValue);
		System.out.println("intValue i : " + intValue);
		System.out.println("longValue l : " + longValue);
		System.out.println();
		
		longValue=45;
		intValue = (int)longValue;
		shortValue = (short) intValue;
		byteValue = (byte) shortValue;
		stringValue = byteValue + "";
		
		//Higher to Lower
		System.out.println("stringValue s : " + stringValue);
		System.out.println("byteValue b : " + byteValue);
		System.out.println("shortValue sh : " + shortValue);
		System.out.println("intValue i : " + intValue);
		System.out.println("longValue l : " + longValue);
		System.out.println();
		
		
		


	}

}
