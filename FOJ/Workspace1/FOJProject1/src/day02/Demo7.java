package day02;

public class Demo7 {

	public static void main(String[] args) {
		int num1 = 10;
		int num2 = 20;
		int temp;
		
		System.out.println("Before Swapping");
		System.out.println("num1 = " + num1 + "\nnum2 = " + num2 + "\n");
		
		//Swapping with temp
		num1 += num2;
		num2 = num1 - num2;
		num1 -= num2;
		
		System.out.println("After Swapping");
		System.out.println("num1 = " + num1 + "\nnum2 = " + num2 + "\n");
		
		//Swapping without temp
		num1 += num2;
		num2 = num1 - num2;
		num1 -= num2;
		
		System.out.println("After Swapping ");
		System.out.println("num1 = " + num1 + "\nnum2 = " + num2 + "\n");
		
		


	}

}
