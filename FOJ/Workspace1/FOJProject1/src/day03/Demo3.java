package day03;

public class Demo3 {
	
	public static int sumOfTwoDigits(int num){
		return (num / 10) + (num % 10);
	}

	public static void main(String[] args) {
		System.out.println(sumOfTwoDigits(53));
		System.out.println(sumOfTwoDigits(23));
	}
	
}
