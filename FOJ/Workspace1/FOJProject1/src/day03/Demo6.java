package day03;

public class Demo6 {
	
	public static int greatestOfThree(int num1, int num2, int num3){
		int max = num1;
		
		if(max < num2){
			max = num2;
		}
		if(max < num3){
			max = num3;
		}
		return max;
		
		
	}

	public static void main(String[] args) {
		System.out.println(greatestOfThree(2,4,6) + " is greater");
		
	}

}
