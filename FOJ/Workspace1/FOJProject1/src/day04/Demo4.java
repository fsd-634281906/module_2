package day04;

public class Demo4 {
	public static boolean isLeapYear(int year){
		if(( year % 400 == 0) || (year % 100 != 0 && year % 4 == 0))
			return true;
		return false;
	}

	public static void main(String[] args) {
		System.out.println(isLeapYear(2000));
		System.out.println(isLeapYear(1900));
		System.out.println(isLeapYear(2020));
	}

}
