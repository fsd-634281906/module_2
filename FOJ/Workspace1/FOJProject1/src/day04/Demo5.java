package day04;

import java.util.Scanner;

public class Demo5 {

	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);


        System.out.print("Enter distance : ");
        double distanceMeters = scan.nextDouble();

        System.out.print("Enter time : ");
        int hours = scan.nextInt();
        int minutes = scan.nextInt();
        int seconds = scan.nextInt();


        double totalTimeSeconds = hours * 3600 + minutes * 60 + seconds;
        double speedMetersPerSecond = distanceMeters / totalTimeSeconds;
        double speedKilometersPerHour = (distanceMeters / 1000) / (totalTimeSeconds / 3600);
        double speedMilesPerHour = (distanceMeters / 1609) / (totalTimeSeconds / 3600);

        System.out.println("Speed:");
        System.out.println("Meters/second: " + speedMetersPerSecond);
        System.out.println("Kilometers/hour: " + speedKilometersPerHour);
        System.out.println("Miles/hour: " + speedMilesPerHour);
        
        scan.close();
		

	}

}
