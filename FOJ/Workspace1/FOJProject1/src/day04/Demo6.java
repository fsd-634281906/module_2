package day04;

public class Demo6 {
	
	public static int isEven(int num){
		
		if (num <= 0)
			return -1;
		
		if(num % 2 == 0)
			return 1;
		return 0;

	}

	public static void main(String[] args) {
		System.out.println(isEven(2));

	}

}
