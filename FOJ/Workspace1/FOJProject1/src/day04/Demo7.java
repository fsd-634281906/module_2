package day04;

public class Demo7 {

	public static void main(String[] args) {
		
		int carprice = 850000;
		int paid = 150000;
		int rem = carprice - paid;
		int roi = 12;
		int time = 48;
		
		double total = (rem * roi)/100;
		double emi = (total + rem)/time;
		
		System.out.println(emi);

	}

}
