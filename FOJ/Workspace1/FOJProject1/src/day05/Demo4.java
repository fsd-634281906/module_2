package day05;

public class Demo4 {
	
	public static int sum(int num){
		if(num == 1)
			return 1;
		
		return num + sum(num - 1);
	}
	
	public static int product(int num){
		if(num == 1)
			return 1;
		
		return num * product(num - 1);
	}
	
	public static int factorial(int num){
		if(num == 1)
			return 1;
		
		return num * factorial(num - 1);
	}



	
	public static void main(String[] args) {
		System.out.println(sum(5));
		System.out.println(product(5));
		System.out.println(factorial(5));

	}

}
