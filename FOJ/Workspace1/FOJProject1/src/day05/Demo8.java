package day05;

public class Demo8 {

	public static void main(String[] args) {
		//		Pattern Programs
		//		----------------
		//		* * * * *
		//		* * * * *
		//		* * * * *
		//		* * * * *
		//		* * * * *
		//				for (int i = 1; i <= 5; i++) {
		//					for (int j = 1; j <= 5; j++) {
		//						System.out.print("* ");
		//					}
		//					System.out.println();
		//				}




		//		1 1 1 1 1 
		//		2 2 2 2 2 
		//		3 3 3 3 3 
		//		4 4 4 4 4 
		//		5 5 5 5 5 
		//
		//
		//				for (int i = 1; i <= 5; i++) {
		//					for (int j = 1; j <= 5; j++) {
		//						System.out.print(i + " ");
		//					}
		//					System.out.println();
		//				}
		//				





		//		1 2 3 4 5 
		//		1 2 3 4 5 
		//		1 2 3 4 5 
		//		1 2 3 4 5 
		//		1 2 3 4 5 
		//
		//
		//
		//				for (int i = 1; i <= 5; i++) {
		//					for (int j = 1; j <= 5; j++) {
		//						System.out.print(j + " ");
		//					}
		//					System.out.println();
		//				}








		//		5 5 5 5 5 
		//		4 4 4 4 4 
		//		3 3 3 3 3 
		//		2 2 2 2 2 
		//		1 1 1 1 1 
		//
		//
		//
		//				for (int i = 5; i >= 1; i--) {
		//					for (int j = 1; j <= 5; j++) {
		//						System.out.print(i + " ");
		//					}
		//					System.out.println();
		//				}






		//		5 4 3 2 1 
		//		5 4 3 2 1 
		//		5 4 3 2 1 
		//		5 4 3 2 1 
		//		5 4 3 2 1 
		//
		//
		//
		//				for (int i = 1; i <= 5; i++) {
		//					for (int j = 5; j >= 1; j--) {
		//						System.out.print(j + " ");
		//					}
		//					System.out.println();
		//				}






		//		A A A A A 
		//		B B B B B 
		//		C C C C C 
		//		D D D D D 
		//		E E E E E 
		//
		//
		//
		//				for (int i = 65; i <= 69; i++) {
		//					for (int j = 1; j <= 5; j++) {
		//						System.out.print(((char)i) + " ");
		//					}
		//					System.out.println();
		//				}






		//
		//		* * * * * 
		//		*       * 
		//		*       * 
		//		*       * 
		//		* * * * * 
		//
		//
		//
		//
		//				for (int i = 1; i <= 5; i++) {
		//					for (int j = 1; j <= 5; j++) {					
		//						if (i == 1 || i == 5) {
		//							System.out.print("* ");
		//						} else {
		//							if (j == 1 || j == 5) {
		//								System.out.print("* ");
		//							} else {
		//								System.out.print("  ");
		//							}
		//						}
		//					}			
		//					System.out.println();
		//				}





		//		* * * * * 
		//		*       * 
		//		*       * 
		//		*       * 
		//		* * * * * 
		//
		//
		//
		//				for (int i = 1; i <= 5; i++) {
		//					for (int j = 1; j <= 5; j++) {
		//						
		//						if (i == 1 || i == 5 || j == 1 || j == 5) 
		//							System.out.print("* ");
		//						
		//						else
		//							System.out.print("  ");
		//						
		//					}			
		//					System.out.println();
		//				}		






		//		# # # # # 
		//		# * * * # 
		//		# * * * # 
		//		# * * * # 
		//		# # # # # 
		//
		//
		//
		//
		//				for (int i = 1; i <= 5; i++) {
		//					for (int j = 1; j <= 5; j++) {
		//						
		//						if (i == 1 || i == 5 || j == 1 || j == 5) 
		//							System.out.print("# ");
		//						
		//						else
		//							System.out.print("* ");
		//						
		//					}			
		//					System.out.println();
		//				}		






		//		1
		//		1 2
		//		1 2 3
		//		1 2 3 4
		//		1 2 3 4 5
		//
		//
		//
		//
		//				for (int i = 1; i <= 5; i++) {
		//					for (int j = 1; j <= i; j++) {
		//						System.out.print(j + " ");
		//					}			
		//					System.out.println();
		//				}		





		//		1
		//		2 2
		//		3 3 3
		//		4 4 4 4
		//		5 5 5 5 5
		//
		//
		//
		//
		//				for (int i = 1; i <= 5; i++) {
		//					for (int j = 1; j <= i; j++) {
		//						System.out.print(i + " ");
		//					}			
		//					System.out.println();
		//				}		








		//		1 2 3 4 5
		//		1 2 3 4
		//		1 2 3
		//		1 2
		//		1
		//
		//
		//				for (int i = 5; i >= 1; i--) {
		//					for (int j = 1; j <= i; j++) {
		//						System.out.print(j + " ");
		//					}			
		//					System.out.println();
		//				}		





		//		5 5 5 5 5
		//		4 4 4 4 
		//		3 3 3 
		//		2 2
		//		1



		//				for (int i = 5; i >= 1; i--) {
		//					for (int j = 1; j <= i; j++) {
		//						System.out.print(i + " ");
		//					}			
		//					System.out.println();
		//				}		
		//			}
		//		}

		//----------------------------------------------------------
		//rhombus
		int n = 5;
		for(int i = 0 ; i < n ; i++){
			for(int j =0 ; j<(2*n)-1 ; j++){
				if( ((i+j)<n) || (i-1+n)<=j){
					System.out.print("* ");
					
				}
				else{ 
					System.out.print("  ");
				}
			}
			System.out.println();
		}
		
		for(int i = 0; i<n-1 ;i++){
			for(int j = 0 ; j<(2*n)-1 ; j++){
				if((i>=j-1) || i>((2*(n-1)-1)-j-1)){
					System.out.print("* ");
				}
				else{
					System.out.print("  ");
				}
			}
			System.out.println();
		}
	}

}

		