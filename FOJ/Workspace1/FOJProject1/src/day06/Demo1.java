package day06;
import java.util.Scanner;

public class Demo1 {

    public static int bill_calculator(){

        Scanner scan = new Scanner(System.in);


        System.out.println("1.coffee - $20");
        System.out.println("2.tea - $20");
        System.out.println("3.beetroot juice - $40");
        System.out.println("4.idly - $50");
        System.out.println("5.Dosa - $50");

        int total = 0;

        while(true){
            System.out.println("Enter item: ");
            String ch = scan.nextLine();

            switch(ch){
                case "1":
                    System.out.println("1.coffee - $20");
                    int price1 = 20;
                    total += price1;
                    System.out.println("Total: " + total);
                    break;
                case "2":
                    System.out.println("2.tea - $20");
                    int price2 = 20;
                    total += price2;
                    System.out.println("Total: " + total);
                    break;
                case "3":
                    System.out.println("3.beetroot juice - $40");
                    int price3 = 40;
                    total += price3;
                    System.out.println("Total: " + total);
                    break;
                case "4":
                    System.out.println("4.idly - $50");
                    int price4 = 50;
                    total += price4;
                    System.out.println("Total: " + total);
                    break;
                case "5":
                    System.out.println("5.Dosa - $50");
                    int price5 = 50; 
                    total += price5;
                    System.out.println("Total: " + total);
                    break;
                default:
                    return total;
            }
        }
    }

    public static void main(String[] args) {
        System.out.println("Grand Total is :" + bill_calculator());
    }
}
