package day07;

public class Demo1 {
	
	public static int getSum(int a[]){
		int sum = 0;
		for(int i = 0; i < a.length; i++){
			sum += a[i];
		}
		return sum;
		
	}
	
	public static long getProduct(int a[]){
		int prod = 1;
		for(int i = 0; i < a.length; i++){
			prod *= a[i];
		}
		return prod;
		
	}
	
	public static String minAndMax(int a[]){
		int max = a[0];
		int min = a[0];
		for(int i = 0; i < a.length; i++){
			if (min > a[i])
				min = a[i];
			if (max < a[i])
				max = a[i];
		}
		return "the minimum is " + min + " and " + "the maximum is " +max;
	}
	
	public static String ascending(int a[]){
		int temp =0;
        for (int i = 0; i < a.length; i++) {     
            for (int j = i+1; j < a.length; j++) {     
               if(a[i] > a[j]) {    
                   temp = a[i];    
                   a[i] = a[j];    
                   a[j] = temp;    
               }     
            }     
        }
        String str = "";
        for(int i = 0; i<a.length;i++){
        	str += a[i]+ " ";
        }
        return str;
       
	}
	

	public static void main(String[] args) {
//		int arr[] = new int[5];
//		
//		arr[0] = 10;
//		arr[1] = 20;
//		arr[2] = 30;
//		arr[3] = 40;
//		arr[4] = 50;
//		
//		System.out.println("a[0] = " + arr[0]);
//		System.out.println("a[1] = " + arr[1]);
//		System.out.println("a[2] = " + arr[2]);
//		System.out.println("a[3] = " + arr[3]);
//		System.out.println("a[4] = " + arr[4]);
		int a[] = {2,1,3,4,5};
		
		for(int i = 0; i < a.length; i++){
			System.out.print(a[i] + " ");
			}
		System.out.println("\n");
		for(int i = a.length-1; i >= 0; i--){
			System.out.print(a[i] + " ");
		}
		System.out.println("\n");
		System.out.println(getSum(a));
		System.out.println(getProduct(a));
		System.out.println(minAndMax(a));
		System.out.println(ascending(a));

	}

}
