package day08;

public class Assmt1 {
//	evenoddsum
    public static void printSumOfEvenAndOdd(int start, int end) {
        int evenSum = 0;
        int oddSum = 0;
        
       
        for (int i = start; i <= end; i++) {
            if (i % 2 == 0) {
                evenSum += i;
            } else {                
                oddSum += i;
            }
        }
        System.out.println("Sum of even numbers in the range [" + start + ", " + end + "]: " + evenSum);
        System.out.println("Sum of odd numbers in the range [" + start + ", " + end + "]: " + oddSum);
    }
	
//Adam Number
	public static int reverseDigits(int num) 
    { 
        int rev = 0; 
        while (num > 0) 
        { 
            rev = rev * 10 + num % 10; 
            num /= 10; 
        } 
        return rev; 
    } 
  
    public static int square(int num) 
    { 
        return (num * num); 
    } 
  
    public static boolean checkAdamNumber(int num) 
    {  
        int a = square(num); 
        int b = square(reverseDigits(num)); 
         
        if (a == reverseDigits(b)) 
        	return true; 
        return false;         
    }
//    Perfect Number
    static boolean isPerfect(int n)
    {
        if (n == 1)
            return false;

        int sum = 1;

        for (int i = 2; i < n; i++) {
            if (n % i == 0) {
                sum += i;
            }
        }
        if (sum == n)
            return true;
 
        return false;
    }
//    Collatz sequence
    public static void printCollatzSequence(int n) {
        System.out.print(n + " ");
        
        while (n != 1) {
            if (n % 2 == 0) {
                n = n / 2;
            } else {
                n = 3 * n + 1;
            }
            System.out.print(n + " ");
        }
    }
//    Armstrong Number
    public static int power(int num, int pow) {
        int result = 1;
        for (int i = 0; i < pow; i++) {
            result *= num;
        }
        return result;
    }

    public static int countDigits(int num) {
        int count = 0;
        while (num != 0) {
            num /= 10;
            count++;
        }
        return count;
    }

    public static boolean isArmstrong(int num) {
        int originalNum = num;
        int sum = 0;
        int numOfDigits = countDigits(num);
        
        while (num != 0) {
            int digit = num % 10;
            sum += power(digit, numOfDigits);
            num /= 10;
        }
        
        return sum == originalNum;
    }
    
    public static void main(String[] args) {
		
		printSumOfEvenAndOdd(1,25);
		System.out.println("\n");
        if (checkAdamNumber(12)) 
        System.out.println("Adam Number"); 
        else
        System.out.println("Not a Adam Number");
        System.out.println("\n");
        if (isPerfect(6))
            System.out.println("It is a perfect number");
        else
            System.out.println(" It is not a perfect number");
        System.out.println("\n");
        printCollatzSequence(6);
        System.out.println("\n");
        if (isArmstrong(153)) {
            System.out.println("It is an Armstrong number.");
        } else {
            System.out.println("It is not an Armstrong number.");
        }
        
        
        
		

	}

}
