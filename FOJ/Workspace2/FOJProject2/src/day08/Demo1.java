package day08;

public class Demo1 {
	
	
	
	public static void showArray(int arr[]){
		for(int i = 0; i < arr.length; i++){
			System.out.print(arr[i] + " ");
		}
	}
	
	public static int[] sortAscending(int arr[]){
		int temp = 0;
		for (int i = 0 ; i < arr.length; i++){
			for (int j = 0; j < arr.length - 1 - i; j++){
				if (arr[j] > arr[j+1]){
					temp = arr[j];
					arr[j] = arr[j + 1];
					arr[j+1] = temp;
				}
			}
			
		}
		return arr;
	}
	
	public static int searchArray(int arr[], int num){
		for( int i = 0; i < arr.length; i++){
			if(arr[i] == num){
				return i;
			}	
		}
		return -1;
		
	}
	
	public static int powerOf2(int num, int pow){
		int result = 1 ;
		for(int i = 0; i < pow; i++){
			result *= num;
		}
		return result;
		
	}

	public static void main(String[] args) {
		int arr[] = {30, 10, 50, 20, 40};
		System.out.println(powerOf2(2,3));
		System.out.println("Before sorting :");
		showArray(arr);
		System.out.println("\n");
	
		arr = sortAscending(arr);
		System.out.println("After sorting: ");
		for(int i = 0; i < arr.length; i++){
			System.out.print(arr[i] + " ");
		}
		System.out.println("\n");
		
		System.out.println(searchArray(arr,99)); // -1
		System.out.println(searchArray(arr,20)); // 1
		System.out.println(searchArray(arr,40)); // 3
		
		
	}
	

}
