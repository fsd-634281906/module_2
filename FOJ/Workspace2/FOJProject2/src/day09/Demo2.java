package day09;

public class Demo2 {

	public static void displayArray(int arr[][]){
		for(int i = 0; i < 3; i++){

			for(int j = 0; j < 3; j++){
				System.out.print(arr[i][j] + " ");
			}
			System.out.println();
		}
		System.out.println();

	}

public static void main(String[] args) {

	int arr1[][] = new int[][]{{10, 20, 30}, {40, 50, 60}, {70, 80, 90}};
	int arr2[][] = new int[][]{{11, 12, 13}, {14, 15, 16}, {17, 18, 19}};
	int arr3[][] = new int[][]{{21, 22, 23}, {24, 25, 26}, {27, 28, 29}};
	
	displayArray(arr1);
	displayArray(arr2);
	displayArray(arr3);

	}
}
