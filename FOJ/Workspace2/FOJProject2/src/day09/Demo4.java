package day09;

public class Demo4 {

	public static void findRowSum(int arr[][]){
		int sum;
		for (int i = 0; i < 3; i++){
			sum = 0;
			for(int j = 0; j < 3; j++){
				sum += arr[i][j];
				System.out.print(arr[i][j] + " ");
			}
			System.out.println(sum);
		}
		System.out.println();


	}
	public static void findRowSum1(int arr[][]){
		int sum;
		for (int i = 0; i < 3; i++){
			sum = 0;
			for(int j = 0; j < 3; j++){
				sum += arr[i][j];
				System.out.println("Row-" + (i + 1) + " Sum = " + sum);
			}
			System.out.println();
			
		}
	
	}
	public static int diagonalSum(int arr[][]){
		int sum=0;
		for (int i = 0; i < 3; i++){
			for(int j = 0; j < 3; j++){
				if(i == j){
					sum+=arr[i][j];
				}
				
			}

		}
		return sum;
	
	}

public static void main(String[] args) {

	int arr1[][] = new int[][]{{10, 20, 30}, {40, 51, 60}, {70, 80, 91}};
	int arr2[][] = new int[][]{{11, 12, 13}, {14, 15, 16}, {17, 18, 19}};
	int arr3[][] = new int[][]{{21, 20, 30}, {30, 41 , 50}, {60, 70, 81}};
	
	findRowSum(arr1);
	findRowSum(arr2);
	findRowSum(arr3);
	
//	findRowSum1(arr1);
//	findRowSum1(arr2);
//	findRowSum1(arr3);
	
	System.out.println(diagonalSum(arr1));
	System.out.println(diagonalSum(arr2));
	System.out.println(diagonalSum(arr3));

	
	
	}
}
