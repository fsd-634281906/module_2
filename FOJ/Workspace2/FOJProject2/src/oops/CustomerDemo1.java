package oops;

public class CustomerDemo1 {

	public static void main(String[] args) {
	
		Customer c1 = new Customer(1001,"SACHIN","TENDULKAR","20-20, EAST, MUMBAI",9999.99);
		System.out.println(c1); //it calls toString()
		c1.setLastName("RAMESH TENDULKAR");
		System.out.println(c1);
		
		Customer c2 = new Customer(1002,"MS","DHONI","50-50, SOUTH, CHENNAI",8888.88);
		System.out.println(c2); //it calls toString()
		c2.setAddress("10-10, NORTH, DELHI");
		System.out.println("Ur Address is : " + c2.getAddress());
		
		Customer c3 = new Customer();
		System.out.println(c3);
		
	}

}
