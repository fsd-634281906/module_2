package oops;

public class Employee {
	int id;
	String name;
	int salary;
	
	public Employee(int id, String name, int salary){
		this.id = id;
		this.name = name;
		this.salary = salary;
	}
	public void details(){
		System.out.println(id);
		System.out.println(name);
		System.out.println(salary);
	}

}
